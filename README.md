Give an example of a program that creates a GUI with at least one button and several textfields. Some of the textfields should be for input and others for output. Make the output textfields uneditable. When the button is clicked, the input fields should be read, some calculation performed and the result displayed in the output textfield(s).


I hope I'm not alone in thinking the class material for this week was less than helpful. I ended up having to do a ton of my own research and YouTube watching to figure out how this actually works. 

Anyways, sticking with my smart home theme, I have created a control panel for lights in a one bedroom apartment (probably should've added bathroom lights but didn't think about it). It allows the user to turn the lights on or off, set the brightness, and get the current brightness settings. I hope you all enjoy!

-Steve