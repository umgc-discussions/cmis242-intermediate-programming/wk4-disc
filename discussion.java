import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Button;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

public class discussion implements ActionListener {

    private JFrame frame;
    private JTextField inputText;
    private JTextField outputText;
    private String input;


    public static void main(String[] args) {
        discussion object = new discussion();
        object.frame.setVisible(true);
        
    }


    private discussion() {
        initialize();
    }


    private void initialize() {
        frame = new JFrame();
        frame.setBounds(200, 200, 500, 500);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Mile to Kilometer Converter");
        frame.setResizable(false);
        frame.getContentPane().setLayout(null);

        inputText = new JTextField();
        inputText.setBounds(10, 30, 440, 100);
        frame.getContentPane().add(inputText);

        outputText = new JTextField();
        outputText.setBackground(Color.BLACK);
        outputText.setForeground(Color.GREEN);
        outputText.setEditable(false);
        outputText.setBounds(10, 300, 440, 100);
        frame.getContentPane().add(outputText);

        Button button = new Button("Convert");
        button.addActionListener(this);
        button.setFont(new Font("SansSerif", Font.PLAIN, 12));
        button.setBounds(10, 150, 440, 100);
        frame.getContentPane().add(button);
    }


    public void actionPerformed(ActionEvent e) {
        double output;
        input = inputText.getText();
        try{
            output = Double.parseDouble(input) * 1.60934;
            outputText.setText(Double.toString(output) + " Kilometers");
        } catch (Exception x) {
            System.out.println("You entered a non number");
            System.exit(0);
        }    
    }
}