public class Main {
    public static void main(String[] args) 
    {
        System.out.println(countDown(2, 0));
    }
    public static int countDown(int n, int result)
    {
        if (n == 0)
            return 0;
        else
            return countDown(n - 1, n + result);
    }
 }
